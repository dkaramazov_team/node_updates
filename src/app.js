const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');

const index = require('./routes/index');
const app = express();

// body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// handlebars
app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

// static
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    res.render('home');
});

// AUTH
app.use('/', index);

module.exports = app;