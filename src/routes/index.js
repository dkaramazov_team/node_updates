const express = require('express');
const router = express.Router();

var Airtable = require('airtable');
var base = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }).base(process.env.AIRTABLE_TABLE_KEY);

router.get('/contact', (req, res) => {    
    res.render('index/contact');
});

router.post('/contact', (req, res) => {    
    base('Contact').create({
        "Contact Form": "[Morgan Scott Films] Contact",
        "Name": req.body.name,
        "Your Fiancé's Name:": req.body.fiance,
        "Email Address:": req.body.email,
        "Where did you hear about us?": req.body.source,
        "Wedding Date:": req.body.date,
        "Wedding Venue/Location:": req.body.location,
        "Wedding Video Budget:": req.body.budget,
        "Tell us any other details about your wedding that may help us!": req.body.details
      }, function(err, record) {
          if (err) { console.error(err); return; }
          if(record){
              res.send('<h1>Thank you for your submission!</h1>');
          }
      });
});
module.exports = router;