const express = require('express');
const exphbs = require('express-handlebars');
const path = require('path');
const passport = require('passport');
const mongoose = require('mongoose');
require('./models/User');
require('./models/Post');
require('./models/Template');
require('./helpers/passport')(passport);

const index = require('./routes/index');
const auth = require('./routes/auth');
const admin = require('./routes/admin');
const template = require('./routes/template');
const app = express();


// map global promise
mongoose.Promise = global.Promise;
// connect to db
mongoose.connect(process.env.MONGO_URI).then(() => {
    console.log("mongodb connected");
}).catch(error => {
    console.log(error);
});

// Setup passport
app.use(passport.initialize());
app.use(passport.session());

// body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// handlebars
app.engine('handlebars', exphbs({
    defaultLayout: 'main'
}));
app.set('view engine', 'handlebars');

// static
app.use(express.static(path.join(__dirname, 'public')));

// AUTH
app.use('/', index);
app.use('/auth', auth);
app.use('/admin', admin);
app.use('/template', template);

module.exports = app;