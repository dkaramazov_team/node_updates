const mongoose = require('mongoose');
const Template = mongoose.model('templates');

exports.getMyTemplates = (req, res) => {
    Template.find({ user: req.user.id })
        .populate('user')
        .then(templates => {
            res.send({
                templates
            });
        })
        .catch(error => console.log(error));
};

exports.getAll = (req, res) => {
    Template.find({ status: 'active' })
        .populate('user')
        .sort({ date: 'desc' })
        .then(templates => {
            res.send({
                templates
            });
        })
        .catch(error => {
            console.log(error);
        });
};

exports.deleteById = (req, res) => {
    Template.remove({ _id: req.params.id })
        .then(() => {
            res.send('Deleted');
        })
        .catch(error => console.log(error));
};

exports.getTemplateById = (req, res) => {
    Template.findOne({
        _id: req.params.id
    })
        .populate('user')        
        .then(template => {
            if (template.status == 'active') {
                res.send({
                    template
                });
            } else {
                if (req.user) {
                    if (req.user.id == template.user._id) {
                        res.send({
                            template
                        });
                    } else {
                        res.send(null);
                    }
                } else {
                    res.send(null);
                }
            }
        });
};

exports.editTemplateById = (req, res) => {
    Template.findOne({
        _id: req.params.id
    })
        .populate('user')        
        .then(template => {
            template.title = req.body.title;
            template.body = req.body.body;            

            template.save()
                .then(template => {
                    res.send({template});
                })
                .catch(error => console.log(error));
        })
        .catch(error => console.log(error));
};

exports.createTemplate = (req, res) => {
    const newTemplate = {
        title: req.body.title,
        body: req.body.body        
    };

    new Template(newTemplate)
        .save()
        .then(template => {
            res.send({template});
        })
        .catch(error => {
            console.log(error);
        });
};