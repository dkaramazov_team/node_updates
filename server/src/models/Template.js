const moment = require('moment');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Shema
const TemplateSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    body: {
        type: String,
        required: true
    },    
    status: {
        type: String,
        default: 'active'
    },        
    user: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    date: {
        type: Date,
        default: moment
    }
});

// Create collection and add schema
mongoose.model('templates', TemplateSchema, 'templates');