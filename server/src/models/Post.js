const moment = require('moment');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Shema
const PostSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    fiance: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: false
    },    
    source: {
        type: String,
        required: false
    },    
    wedding_date: {
        type: String,
        required: false
    },    
    location: {
        type: String,
        required: false
    },    
    budget: {
        type: Number,
        required: false
    },    
    details: {
        type: String,
        required: false
    },    
    created_date: {
        type: Date,
        default: moment
    }
});

// Create collection and add schema
mongoose.model('posts', PostSchema, 'posts');