const express = require('express');
const router = express.Router();

const { checkAuthenticated, checkAdmin } = require('../helpers/auth');
const { getAll, getTemplateById, getMyTemplates, createTemplate, editTemplateById } = require('../controllers/templateController');

router.get('/', (req, res) => {    
    res.render('index/template');
});

router.get('/', getAll);

router.get('/user/:id', getMyTemplates);

router.get('/:id', getTemplateById);

router.put('/:id', editTemplateById);

router.post('/', createTemplate);

module.exports = router;